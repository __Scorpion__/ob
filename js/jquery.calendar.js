( function(){
    $( function(){
        $.each($('.calendar'), function () {
            new Calendar( $(this) );

        });
    } );

    var Calendar = function (obj) {

        //private properties
        var _self = this,
            _calendar = obj.find('.calendar__item'),
            _data = _calendar.data('calendar'),
            _partlyAvailable = {},
            _unavailable = {},
            _obj = obj,
            _widget = null;

        //private methods
        var _addEvents = function () {

            },
            _addDatePicker = function(){
                _calendar.datepicker({
                    onChangeMonthYear: function(year, month){
                        setTimeout(function(){
                            _setData(year, month);
                        }, 50)
                    }
                });

                _widget = _calendar.datepicker( "widget" );

                _setData();
            },
            _getData = function(){

                if(_data.partly_available) {
                    $.each(_data.partly_available, function () {
                        var curDate = this.date.split('.'),
                            curMonth= parseInt( curDate[1] ),
                            curYear= parseInt( curDate[2] ),
                            curDay = parseInt( curDate[0] );

                        if(!_partlyAvailable[curYear]){
                            _partlyAvailable[curYear] = {}
                        }
                        if(!_partlyAvailable[curYear][curMonth]){
                            _partlyAvailable[curYear][curMonth] = [];
                        }
                        _partlyAvailable[curYear][curMonth].push( curDay );

                    });
                }
                if(_data.unavailable) {
                    $.each(_data.unavailable, function () {
                        var curDate = this.date.split('.'),
                            curMonth= parseInt( curDate[1] ),
                            curYear= parseInt( curDate[2] ),
                            curDay = parseInt( curDate[0] );

                        if(!_unavailable[curYear]){
                            _unavailable[curYear] = {}
                        }
                        if(!_unavailable[curYear][curMonth]){
                            _unavailable[curYear][curMonth] = [];
                        }
                        _unavailable[curYear][curMonth].push( curDay );

                    });
                }
            },
            _init = function () {
                _getData();
                _addDatePicker();
                _addEvents();
            },
            _setData = function(year,month){
                var date = _calendar.datepicker('getDate'),
                    month = month || date.getMonth() + 1,
                    year =  year || date.getFullYear(),
                    unavailableDays = [],
                    partlyAvailableDays = [],
                    links = _obj.find('.ui-state-default');


                if(_unavailable[year]){
                    if(_unavailable[year][month]){
                        unavailableDays = _unavailable[year][month];
                    }
                }
                if(_partlyAvailable[year]){
                    if(_partlyAvailable[year][month]){
                        partlyAvailableDays = _partlyAvailable[year][month];
                    }
                }

                $.each(unavailableDays, function () {
                    var curDay = this;

                    $.each(links, function () {
                        var curLnk = $(this);

                        if(curLnk.text() == curDay){
                            curLnk.addClass( 'calendar__unavailable' );
                            return false;
                        }
                    });
                });
                $.each(partlyAvailableDays, function () {
                    var curDay = this;

                    $.each(links, function () {
                        var curLnk = $(this);

                        if(curLnk.text() == curDay){
                            curLnk.addClass( 'calendar__partly-available' );
                            return false;
                        }
                    });
                });

            };

        //public properties

        //public methods


        _init();
    };
} )();