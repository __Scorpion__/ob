$(function(){
    $( 'select' ).each( function(){
        new AresSelect( {
            obj: $( this ),
            optionType: 1,
            showType: 2
        } );
    } );
    $( '.tickets' ).each( function(){
        new Tickets( $(this) );
    } );

    if($('.share-email').length){
        new MailShare($('.share-email'));
    }


    $('.sort-by-btn').on('click', function(){
        var curElem = $(this);
        if(curElem.hasClass('active')){
            curElem.removeClass('active');
        }
        else{
            curElem.addClass('active');
        }
        return false;
    });
    if($('.events-list').length){
        new SelectEvent($('.events-list'));
    }
    $('.concept__video__play').on('click', function(){

        var curElem = $(this),
        videoPlayer = $('.video-player');
        videoPlayer[0].play();
        curElem.fadeOut();

    });

    $('.video-player').on('mouseover', function(){
        $(this).attr("controls", "controls");
    });

    $('.video-player').on('mouseleave', function(){
        $(this).removeAttr("controls");
    });

    if($( ".datepicker").length){
        $( ".datepicker" ).datepicker();
    }
    if($( "#slider-range").length) {
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 500,
            values: [75, 300],
            slide: function (event, ui) {
                $("#amount").val(  ui.values[0] + "€ - " + ui.values[1]+"€");
            }
        });

        $("#amount").val($("#slider-range").slider("values", 0) +
        "€ - " + $("#slider-range").slider("values", 1)+"€");

    }

    $('.show-sort').on('click', function(){

        var curElem = $(this);
        if(curElem.next().css('display') == 'block'){
            curElem.next().slideUp();
        }
        else{
            curElem.next().slideDown();
        }

        return false;

    });

    if($('.carusel-slider').length){
        var swiper = new Swiper('.carusel-slider .swiper-container', {
            pagination: '.swiper-pagination',
            slidesPerView: 'auto',
            loopedSlides: 10,
            centeredSlides: true,
            spaceBetween: 30,
            nextButton: '.swiper-button-next2',
            prevButton: '.swiper-button-prev2',
            loop: true
        });
    }

});
$( window ).on({
    load: function(){
        $( '.artist' ).each( function(){
            new Artist( $(this) );
        } );
    }
});

var Tickets = function (obj) {

    //private properties
    var _self = this,
        _selects = obj.find( 'select' ),
        _total = obj.find('.organize__table-total > span > span'),
        _obj = obj;

    //private methods
    var _addEvents = function () {
            _obj.on('click', '.organize__table_del', function(){
                var curLine = $( this ).parents( 'li' ).eq( 0 );
                curLine.addClass('organize__table_hide');

                setTimeout(function(){
                    curLine.remove();
                    _self.calculateTotal();
                }, 300);

                return false
            } );
            _obj.on('click', 'li', function(){
                if(!obj.hasClass('uneditable')){
                    var curItem = $(this)
                    setTimeout(function(){
                        curItem.addClass('organize__table_edit');
                        curItem.find('>div').eq(0).find('>input').focus();
                    },1);
                }


            } );
            _selects.on({
                change: function () {
                    _self.calculateTotal();
                }
            });
            _obj.parent().find('.btn_add').on('click',  function(){
                var list = _obj.find('.organize__table-content');

                var newItem = $('<li class="organize__table_edit organize__table_hide">\
                <div>\
                <div>\
                <a href="#" class="organize__table_del"></a>\
                </div>\
                </div>\
                <div>\
                <input type="text" value="Ticket Name"/>\
                <span>\
                <input type="text" value="Ticket Description..."/>\
                </span>\
                </div>\
                <div>\
                <label>Quantity:</label>\
                <select>\
                <option value="30">30</option>\
                <option value="50" selected>50</option>\
                </select>\
                </div>\
                <div>\
                <span>Price:</span>\
                <input type="text" value="2€"/>\
                </div>\
                </li>');

                _obj.find('.organize__table_edit').removeClass('organize__table_edit');


                new AresSelect( {
                    obj: newItem.find('select'),
                    optionType: 1,
                    showType: 2
                } );

                list.append(newItem);

                setTimeout(function(){
                    newItem.removeClass('organize__table_hide');
                },20);

                return false;

            } );
            $('body').on({
                click: function () {
                    _obj.find('.organize__table_edit').removeClass('organize__table_edit');
                }
            });
        },
        _init = function () {
            _addEvents();
            _self.calculateTotal();

        };

    //public methods
    _self.calculateTotal = function(){
        if(_total){
            var total = 0;

            $.each(_obj.find('.organize__table-content li'), function () {
                var curItem = $(this),
                    curCount = curItem.find('select'),
                    curPrice = curItem.find('input[type="hidden"]');

                total += ( curCount.val() * curPrice.val() );
            });

            _total.text(total);
        }
    };

    _init();
};
var AresSelect = function( params ){
    this.obj = params.obj;
    this.optionType = params.optionType || 0;
    this.showType = params.showType || 0;
    this.visible = params.visible || 5;

    this.init();
};
AresSelect.prototype = {
    init: function(){
        var self = this;

        self.core = self.core();
        self.core.build();
    },
    core: function(){
        var self = this;

        return {
            build: function(){
                self.core.start();
                self.core.controls();
            },
            start: function(){
                self.device = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
                self.text = $( '<span class="ares-select__item"></span>' );
                self.wrap = $( '<div class="ares-select"></div>' );
                self.class = self.obj.attr('class');
                self.window = $( window );
                self.opened = false;

                if(self.class){
                    self.wrap.attr('class', 'ares-select '+ self.class)
                }

                self.core.addWrapper();
                if( !self.optionType || self.device ){
                    self.core.setMobileView();
                } else if( self.optionType == 1 ){
                    self.core.setCustom1();
                }

                self.obj[ 0 ].customSelect = this;
            },
            setMobileView: function(){
                self.wrap.addClass( 'ares-select_mobile' );
            },
            setCustom1: function(){
                self.wrap.addClass( 'ares-select_custom' );
            },
            destroy: function(){
                self.text.remove();
                self.wrap.unwrap();
            },
            addWrapper: function(){
                var curText = '',
                    curImg = '';

                self.obj.css( {
                    opacity: 0
                } );

                self.obj.wrap( self.wrap );
                self.wrap = self.obj.parent();
                self.obj.before( self.text );
                self.obj.find( 'option' ).each( function(){
                    var curItem = $( this );

                    if( curItem.attr( 'selected' ) == 'selected' ){
                        curText = curItem.text();
                        if( curItem.data('image') ){
                            curImg = curItem.data('image');
                        }
                    }
                } );

                if( curText == '' ){
                    curItem = self.obj.find( 'option').eq( 0 )
                    curText =  curItem.text();
                    if( curItem.data('image') ){
                        curImg = curItem.data('image');
                    }
                }
                self.text.html('');
                if(curImg!=''){
                    self.text.append('<img src="' + curImg + '" alt="">');
                }
                self.text.append( curText );
            },
            showPopup: function(){
                var list = $( '<ul></ul>'),
                    curScroll = self.window.scrollTop(),
                    offset = self.wrap.offset(),
                    maxHeight = 0,
                    curIndex = self.obj.find( 'option:selected' ).index(),
                    id = Math.round( Math.random() * 1000 );

                if( self.opened ){
                    self.popup.remove();
                }
                self.opened = true;

                self.popup = $( '<div class="ares-select__popup" id="ares-select__popup' + id + '"></div>' );

                if( self.class ){
                    self.popup.addClass( self.class );
                }

                self.obj.find( 'option' ).each( function(i){
                    var curItem = $( this),
                        curImg = '';

                    if( curItem.data('image') ){
                        curImg = curItem.data('image');
                    }
                    if( i == curIndex ){
                        if(curImg !=''){
                            list.append( '<li class="active"><img src="'+ curImg +'" alt="">' + curItem.text() + '</li>' );
                        } else {
                            list.append( '<li class="active">' + curItem.text() + '</li>' );
                        }

                    } else {
                        if(curImg !=''){
                            list.append( '<li><img src="'+ curImg +'" alt="">' + curItem.text() + '</li>' );
                        } else {
                            list.append( '<li>' + curItem.text() + '</li>' );
                        }
                    }

                } );

                self.popup.append( list );
                $( 'body' ).append( self.popup );

                self.popup.css( {
                    width: self.wrap.outerWidth(),
                    left: offset.left,
                    top: offset.top + self.wrap.outerHeight()
                } );

                maxHeight = self.popup.outerHeight();
                if( maxHeight > self.popup.find( 'li' ).eq( 0 ).outerHeight() * self.visible ){
                    self.popup.height(self.popup.find( 'li' ).eq( 0 ).outerHeight() * self.visible);
                    self.scroll = new IScroll('#ares-select__popup' + id, {
                        scrollX: false,
                        scrollY: true,
                        click: true,
                        zoom:true,
                        mouseWheel: true,
                        scrollbars: 'custom',
                        probeType: 3,
                        interactiveScrollbars: true
                    });
                }

                if( self.showType == 1 ){
                    self.popup.css( {
                        display: 'none'
                    } );
                    self.popup.slideDown( 300, function(){
                        if( self.scroll ) {
                            self.scroll.refresh();
                        }
                    } );
                } else if( self.showType == 2 ) {
                    self.popup.css( {
                        opacity: 0.1
                    } );
                    self.popup.animate( { opacity: 1 },300, function(){
                        if( self.scroll ) {
                            self.scroll.refresh();
                        }
                    } );
                }

                self.popup.find( 'li' ).on( {
                    'click': function( event ){
                        var event = event || window.event,
                            index = $( this ).index();

                        if (event.stopPropagation) {
                            event.stopPropagation()
                        } else {
                            event.cancelBubble = true
                        }


                        self.obj.val( self.obj.find( 'option' ).eq( index).attr( 'value' ) );
                        self.obj.trigger( 'change' );
                        self.core.hidePopup();

                    }
                } );

            },
            hidePopup: function(){
                self.opened = false;
                if( !self.showType ){
                    self.popup.css( {
                        display: 'none'
                    } );
                } else if( self.showType == 1 ){
                    self.popup.stop( true, false ).slideUp( 300, function(){
                        self.popup.remove();
                    } );
                } else if( self.showType == 2 ) {
                    self.popup.stop( true, false ).fadeOut( 300, function(){
                        self.popup.remove();
                    } );
                }
            },
            controls: function() {
                self.obj.on( 'change', function() {
                    var curItem = $( this ).find( 'option:selected'),
                        curImg = ''

                    if( curItem.data('image') ){
                        curImg = curItem.data('image');
                    }
                    self.text.html('');
                    console.log(curImg);
                    if(curImg!=''){
                        self.text.append('<img src="' + curImg + '" alt="">');
                    }
                    self.text.append( curItem.text() );
                } );

                if( self.optionType == 1 && !self.device ){
                    self.wrap.on( {
                        'click': function(event){
                            var event = event || window.event;

                            if (event.stopPropagation) {
                                event.stopPropagation()
                            } else {
                                event.cancelBubble = true
                            }

                            if( self.opened ){
                                self.core.hidePopup();
                            } else {
                                self.core.showPopup();
                            }

                        }
                    } );
                    $( 'body' ).on( {
                        'click': function(){
                            if( self.opened ){
                                self.core.hidePopup();
                            }
                        }
                    } );
                }
            }
        };
    }
};
var Artist = function (obj) {

    //private properties
    var _self = this,
        _obj = obj,
        _swiper = null,
        _popupOpen = obj.find('.artist__gallery .popup__open');

    //private methods
    var _addEvents = function () {
            _popupOpen.on({
                click: function () {
                    var curIndex = _popupOpen.index(this) + 1;

                    setTimeout(function(){
                        _swiper.update(true);
                        _swiper.slideTo(curIndex, 0);
                        $(window).trigger('resize');
                        $('.swiper-container').trigger('resize');
                    },1);
                }
            });
        },
        _init = function () {
            _initSwiper();
            _addEvents();
        }
        _initSwiper = function(){

            _swiper = new Swiper('.popup__gallery-sw .swiper-container', {
                speed: 400,
                spaceBetween: 0,
                loop: true,
                preventClicks:false,
                preventClicksPropagation: false,
                paginationClickable: true,
                slidesPerView: 1,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                onSlideChangeStart: function(){

                },
                onSlideChangeEnd: function(){

                }
            });
        };

    _init();
};

var MailShare = function (obj) {

    //private properties
    var _self = this,
        _obj = obj,
        _addbtn = _obj.find('.add-line'),
        _removebtn = obj.find('.delete-line');

    //private methods
    var _addEvents = function () {

            _obj.on( 'click', '.add-line', function(){
                var curElem = $(this),
                    _newLine = $('<li><input type="email" class="site-inuput" placeholder="Email Address"><button class="delete-line" type="button">delete</button><button class="add-line" type="button">add</button></li>');
                _newLine.css({
                    'display': 'none'
                });
                _newLine.insertAfter(curElem.parent());
                _newLine.fadeIn();
                return false;
            });

            _obj.on( 'click', '.delete-line', function(){
                var curElem = $(this);
                curElem.parent().fadeOut(function(){
                    curElem.parent().remove();
                });

                return false;
            });

        },
        _init = function () {
            _addEvents();
        };

    _init();
};

var SelectEvent = function (obj) {

    //private properties
    var _self = this,
        _obj = obj,
        _items = obj.find('>li');

    //private methods
    var _addEvents = function () {

        _items.on('click', function(e){

            var curElem = $(this);
            if (e.ctrlKey) {
                if(curElem.hasClass('active')){
                    curElem.removeClass('active');
                    _checkNumber();
                }
                else{
                    curElem.addClass('active');
                    _checkNumber();
                }
            }
        });

    },
    _checkNumber = function () {

        var counters = obj.find('>li.active .events-list__img > span'),
            i;

        for(i=0;i<counters.length;i++){
            counters.eq(i).text(i+1);
        }

    },
    _init = function () {

        _addEvents();

    };

    _init();
};