( function(){
    $( function(){
        $.each($('.registration'), function () {
            new Registration( $(this) );
        });
    } );

    var Registration = function (obj) {

        //private properties
        var _self = this,
            _step2 = obj.find('.registration__second-step'),
            _step1 = obj.find('.registration__first-step'),
            _btnNext = obj.find('.registration__first-step button'),
            _obj = obj;

        //private methods
        var _addEvents = function () {
                _btnNext.on({
                    click: function () {

                        _step1.css( { display: 'none' } );
                        _step2.css( { display: 'block' } );

                        return false;
                    }
                });
            },
            _init = function () {
                _addEvents();
            };

        //public properties

        //public methods


        _init();
    };
} )();