(function(){
    var invoices = null;
    $(function(){
        $.each($('.invoices'), function () {
            invoices = new Invoices( $(this) );
        });
    });

    var Invoices = function (obj) {

        //private properties
        var _self = this,
            _items = obj.find( '.invoices__item' ),
            _obj = obj;

        //private methods
        var _addEvents = function () {
                _self.addCheckEvent( _items );
            },
            _init = function () {
                _addEvents();
            };

        //public properties

        //public methods
        _self.addCheckEvent = function( items ){
            var curItem = null;

            $.each(items, function () {
                curItem = $( this );

                curItem.on({
                    click: function () {
                        _self.checkItem( $( this ) );
                    }
                });
            });
        };
        _self.checkItem = function( item ){
            var input = item.find('input'),
                check = false;
            if(item.hasClass('invoices__item_active')){
                item.removeClass('invoices__item_active');
                input[0].checked = false;

            } else {
                item.addClass('invoices__item_active');
                input[0].checked = true;
            }
            if($('.invoices__item_active').length){
                $('.invoices__title').addClass('invoices__title_selected');
            } else {
                $('.invoices__title').removeClass('invoices__title_selected');

            }

        };



        _init();
    };
})();