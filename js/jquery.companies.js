(function(){
    $( function(){
        $.each($('.companies'), function () {
            new Companies( $(this) );

        });
    } );

    var Companies = function (obj) {

        //private properties
        var _self = this,
            _addNewCompanyBtn = obj.find('.companies__new'),
            _contentWrapper = obj.find('.companies__content'),
            _obj = obj;

        //private methods
        var _addEvents = function () {
                _obj.on('click','.companies__modify', function(){
                    $( this ).parents('.companies__item').addClass('companies__item_edit');
                });
                _addNewCompanyBtn.on({
                    click: function () {
                        _appendNewCompany();
                    }
                });
            },
            _appendNewCompany = function(){
                var newCompany = $('<li class="companies__item companies__item_edit">\
                <div>\
                    <div></div>\
                </div>\
                <div class="companies__static">\
                    <span class="companies__name"></span>\
                </div>\
                <div class="companies__static">\
                    <span>VAT</span>\
                    <div>BE 0121.785.912</div>\
                </div>\
                <div class="companies__static">\
                    <button class="btns btns_3 companies__modify"></button>\
                </div>\
                <div class="companies__edit">\
                    <form action="#">\
                        <input type="hidden" id="company3" value="1">\
                        <fieldset>\
                            <input type="text" class="site-inuput" id="company3_name" value=""/>\
                            <label for="company3_name">Company Name</label>\
                        </fieldset>\
                        <fieldset>\
                            <input type="text" class="site-inuput" id="company3_vat" value=""/>\
                            <label for="company3_vat">VAT <span>(Value-Added Tax)</span></label>\
                        </fieldset>\
                        <fieldset>\
                            <input type="email" class="site-inuput" id="company3_email" value=""/>\
                            <label for="company3_email">Email Address</label>\
                        </fieldset>\
                        <fieldset>\
                            <input type="text" class="site-inuput" id="company3_sector" value=""/>\
                            <label for="company3_sector">Sector</label>\
                        </fieldset>\
                        <fieldset class="companies__address">\
                            <input type="text" class="site-inuput" id="company3_address" value=""/>\
                            <label for="company3_address">Address</label>\
                        </fieldset>\
                        <fieldset>\
                            <button class="btns btns_4">Save Changes</button>\
                        </fieldset>\
                    </form>\
                </div>\
                </li><!-- /companies__item -->');

                _contentWrapper.append( newCompany );
            },
            _init = function () {
                _addEvents();
            };

        //public properties

        //public methods


        _init();
    };
})();