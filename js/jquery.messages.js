(function(){
    var messages = null;
    $(function(){
        $.each($('.messages'), function () {
            messages = new Messages( $(this) );
        });
    });

    var Messages = function (obj) {

        //private properties
        var _self = this,
            _items = obj.find( '.messages__item' ),
            _swiper = null,
            _popup__open = obj.find('.popup__open'),
            _obj = obj;

        //private methods
        var _addEvents = function () {
                _self.addCheckEvent( _items );
                _popup__open.on({
                    click: function () {
                        var curIndex = _popup__open.index(this) +1;

                        setTimeout(function(){
                            _swiper.update(true);
                            _swiper.slideTo(curIndex, 0);

                        },1);
                    }
                });
                $('.swiper-container').on( 'click','.btns_reply', function(){
                    $( this ).parents('.message__card').addClass('message_opened');
                    return false;
                    //
                } );
            },
            _addSwiper = function(){
                _swiper = new Swiper('.swiper-container', {
                    speed: 400,
                    spaceBetween: 0,
                    loop: true,
                    preventClicks:false,
                    preventClicksPropagation: false,
                    paginationClickable: true,
                    slidesPerView: 1,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    onSlideChangeStart: function(){
                        $('.message__card').removeClass('message_opened');
                    },
                    onSlideChangeEnd: function(){

                        var titles = $('.message__card-title');
                            curIndex = _swiper.activeIndex,
                            nextIndex = curIndex+ 1,
                            prevIndex = curIndex - 1,
                            titleNext = null,
                            titlePrev = null,
                            namePrev = null,
                            nameNext = null,
                            timePrev = null,
                            timeNext = null;

                        if( nextIndex == titles.length ){
                            nextIndex = 0;
                        }
                        if( prevIndex < 0 ){
                            prevIndex = titles.length - 1;
                        }

                        titleNext = titles.eq(nextIndex);
                        titlePrev = titles.eq(prevIndex);
                        namePrev = titlePrev.find('h2').text();
                        nameNext = titleNext.find('h2').text();
                        timePrev = titlePrev.find('time').text();
                        timeNext = titleNext.find('time').text();

                        $('.message__btn-prev').find('div').text(namePrev);
                        $('.message__btn-prev').find('span').text(timePrev);
                        $('.message__btn-next').find('div').text(nameNext);
                        $('.message__btn-next').find('span').text(timeNext);

                    }
                });
            },
            _init = function () {
                _addSwiper();
                _addEvents();
            };

        //public properties

        //public methods
        _self.addCheckEvent = function( items ){
            var curItem = null;

            $.each(items, function () {
                curItem = $( this );

                curItem.on({
                    click: function () {
                        _self.checkItem( $( this ) );
                    }
                });
            });
        };
        _self.checkItem = function( item ){
            var input = item.find('input'),
                check = false;
            if(item.hasClass('messages__item_active')){
                item.removeClass('messages__item_active');
                input[0].checked = false;

            } else {
                item.addClass('messages__item_active');
                input[0].checked = true;
            }
            if($('.messages__item_active').length){
                $('.messages__title').addClass('messages__title_selected');
            } else {
                $('.messages__title').removeClass('messages__title_selected');

            }

        };



        _init();
    };
})();